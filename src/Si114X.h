/*
 * ----------------------------------------------------------------------------
 *            _____ _           _                   _
 *           | ____| | ___  ___| |_ _ __ ___  _ __ (_) ___
 *           |  _| | |/ _ \/ __| __| '__/ _ \| '_ \| |/ __|
 *           | |___| |  __/ (__| |_| | | (_) | | | | | (__
 *           |_____|_|\___|\___|\__|_|  \___/|_| |_|_|\___|
 *            ____                   _   ____
 *           / ___|_      _____  ___| |_|  _ \ ___  __ _ ___
 *           \___ \ \ /\ / / _ \/ _ \ __| |_) / _ \/ _` / __|
 *            ___) \ V  V /  __/  __/ |_|  __/  __/ (_| \__ \
 *           |____/ \_/\_/ \___|\___|\__|_|   \___|\__,_|___/
 *
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <pontus@sweetpeas.se> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Pontus Oldberg
 * ----------------------------------------------------------------------------
 *
 *
 * After reset the Si114X device can be used in a polled mode to retreive
 * information about the PS and ALS status.
 */
#ifndef __GUARD_SI114X_H__
#define __GUARD_SI114X_H__

#include <Arduino.h>
#include <Wire.h>

#define LOOP_TIMEOUT_MS           100

#define IR_ADDRESS                0x5A

//
// I2C register addresses
//
#define PART_ID                   0x00
#define REV_ID                    0x01
#define SEQ_ID                    0x02  //Si114x-A11 (MAJOR_SEQ=1, MINOR_SEQ=1)
#define INT_CFG                   0x03
#define IRQ_ENABLE                0x04
#define IRQ_MODE1                 0x05
#define IRQ_MODE2                 0x06
#define HW_KEY                    0x07

#define MEAS_RATE                 0x08
#define ALS_RATE                  0x09
#define PS_RATE                   0x0A

#define ALS_LOW_TH0               0x0B
#define ALS_LOW_TH1               0x0C
#define ALS_HI_TH0                0x0D
#define ALS_HI_TH1                0x0E

#define PS_LED21                  0x0F
#define PS_LED3                   0x10

#define PS1_TH0                   0x11
#define PS1_TH1                   0x12
#define PS2_TH0                   0x13
#define PS2_TH1                   0x14
#define PS3_TH0                   0x15
#define PS3_TH1                   0x16
#define PARAM_WR                  0x17
#define COMMAND                   0x18

#define RESPONSE                  0x20
#define IRQ_STATUS                0x21

#define ALS_VIS_DATA0             0x22
#define ALS_VIS_DATA1             0x23
#define ALS_IR_DATA0              0x24
#define ALS_IR_DATA1              0x25

#define PS1_DATA0                 0x26
#define PS1_DATA1                 0x27
#define PS2_DATA0                 0x28
#define PS2_DATA1                 0x29
#define PS3_DATA0                 0x2A
#define PS3_DATA1                 0x2B

#define AUX_DATA0                 0x2C
#define AUX_DATA1                 0x2D

#define PARAM_RD                  0x2E
#define CHIP_STAT                 0x30
#define ANA_IN_KEY                0x3B

//
// ram addresses
//
#define I2C_ADDR                  0x00
#define CHLIST                    0x01
#define PSLED12_SELECT            0x02
#define PSLED3_SELECT             0x03
#define PS_ENCODING               0x05
#define ALS_ENCODING              0x06
#define PS1_ADCMUX                0x07
#define PS2_ADCMUX                0x08
#define PS3_ADCMUX                0x09
#define PS_ADC_COUNTER            0x0A
#define PS_ADC_GAIN               0x0B
#define PS_ADC_MISC               0x0C
#define ALS_IR_ADCMUX             0x0E
#define AUX_ADCMUX                0x0F
#define ALS_VIS_ADC_COUNTER       0x10
#define ALS_VIS_ADC_GAIN          0x11
#define ALS_VIS_ADC_MISC          0x12
#define ALS_HYST                  0x16
#define PS_HYST                   0x17
#define PS_HISTORY                0x18
#define ALS_HISTORY               0x19
#define ADC_OFFSET                0x1A
#define LED_REC                   0x1C
#define ALS_IR_ADC_COUNTER        0x1D
#define ALS_IR_ADC_GAIN           0x1E
#define ALS_IR_ADC_MISC           0x1F

//
// Si114x Commands
//
#define CMD_QUERY                 0X80
#define CMD_SET                   0XA0
#define CMD_AND                   0xC0
#define CMD_OR                    0xE0
#define CMD_NOP                   0X00
#define CMD_RESET                 0X01
#define CMD_BUSADDR               0X02
#define CMD_PS_FORCE              0X05
#define CMD_GET_CAL               0X12
#define CMD_ALS_FORCE             0X06
#define CMD_PSALS_FORCE           0X07
#define CMD_PS_PAUSE              0X09
#define CMD_ALS_PAUSE             0X0A
#define CMD_PSALS_PAUSE           0X0B
#define CMD_PS_AUTO               0X0D
#define CMD_ALS_AUTO              0X0E
#define CMD_PSALS_AUTO            0X0F

#define PS1                       0
#define PS2                       1
#define PS3                       2

#define PS_HISTORY_2_SAMPLES      0x03
#define PS_HISTORY_3_SAMPLES      0x07
#define PS_HISTORY_8_SAMPLES      0xff

// Call back function
typedef void (*sensor_triggered)(void);

// The sensor class
class Si114X {
public:
  void begin() {}
  void begin(uint32_t, uint32_t);

  Si114X();

  int resetDevice();
  int psAlsPause();
  int pauseAll();
  int setRates(uint16_t meas_rate, uint16_t ps_rate = 0, uint16_t als_rate = 0);
  int setPsRate(uint16_t ps_rate);
  int setAlsRate(uint16_t als_rate);
  int setPsHysteresis(uint16_t val);
  int setPsHistory(uint8_t val);
  int startPsAutonomus(uint8_t channel, uint8_t int_pin, sensor_triggered cb);
  int startAlsAutonomus(uint8_t channel, uint8_t int_pin, sensor_triggered cb);
  int readPsData(int channel, uint16_t *val);
  int clearInterrupts(uint8_t val);
  int readInterrupts(uint8_t *val);

private:
  int readParam(uint8_t reg, uint8_t *val);
  int writeParam(uint8_t reg, uint8_t value, uint8_t *ret = NULL);
  int andParam(uint8_t reg, uint8_t value, uint8_t *ret = NULL);
  int orParam(uint8_t reg, uint8_t value, uint8_t *ret = NULL);
  uint16_t uncompress(uint8_t input);
  uint8_t compress(uint16_t input);
  int writeCommand(uint8_t command);
  int waitForSleep();
};

#endif // __GUARD_SI114X_H__
