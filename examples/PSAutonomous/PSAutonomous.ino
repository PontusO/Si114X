/*
 * Short Si114X test program
 * 
 * It will start the sensor in autonomous proximity sensing mode
 * and print a short string when ever an object enters or exits the
 * set range.
 * 
 */
#include <Wire.h>
#include <Si114X.h>

#define SDA_PIN       2
#define SCL_PIN       14
#define SI1141_INT    13

#define PRINT_TIMER   500

Si114X si114x;

boolean clearInterrupt = false;
uint32_t mTimer;

/*
 * Call back function for the interrupt from the sensor
 * Called in interrupt context so it must execute as quick as possible.
 */
void proximityCb(void)
{
  clearInterrupt = true;
}


void setup()
{
  int status = 0;

  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);

  Serial.begin(115200);
  Serial.println(F("\n\nTesting the Si114X library !"));
  
  Wire.begin(SDA_PIN, SCL_PIN);
  si114x.begin();

  // Reset the device before doing anything else.
  status = si114x.resetDevice();
  if (status) {
    Serial.print(F("Failed to reset the Si1141-M01 module. Error code: "));
    Serial.println(status);
  } else {
    Serial.println(F("Si1141-M01 reset successful."));
  }

  // Set device rate to 4Hz and PS to do a measurement each time
  status = si114x.setRates(0x1F40, 1);
  if (status) {
    Serial.print(F("Failed to set proper conversion rate. Error code: "));
    Serial.println(status);
  } else {
    Serial.println(F("Device rate set."));
  }

  // Start autonomous proximity sensing
  status = si114x.startPsAutonomus(PS1, SI1141_INT, proximityCb);
  if (status) {
    Serial.print(F("Failed to set autonomous mode. Error code: "));
    Serial.println(status);
  } else {
    Serial.println(F("Autonomous mode started."));
  }
  mTimer = millis() + PRINT_TIMER;
}

void loop()
{
  uint16_t ps1;

  if (clearInterrupt) {
    uint8_t irqs;
    
    clearInterrupt = false;
    si114x.readInterrupts(&irqs);
    si114x.clearInterrupts(irqs);

    Serial.println(F("Hepp, got an interrupt from the infamous Si114X chip !"));
  }
}
